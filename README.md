# Desafio Angular (My-Store)

A simple project of store.

Made with Angular Material and coffee. :D

## Packages Used:

1. [Angular CLI](https://github.com/angular/angular-cli) version 10.0.0.

2. [Angular Material](https://material.angular.io/) version 10.0.0.

## Preview:

<img src="https://gitlab.com/william.franco/angularmaterialstore/-/blob/master/src/screenshots/home_screen.png" height="400em" />

<img src="https://gitlab.com/william.franco/angularmaterialstore/-/blob/master/src/screenshots/ranking_screen.png" height="400em" />

<img src="https://gitlab.com/william.franco/angularmaterialstore/-/blob/master/src/screenshots/about_screen" height="400em" />
