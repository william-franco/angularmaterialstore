import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { RankingModel } from './ranking-model';

@Injectable({
  providedIn: 'root'
})
export class RankingLojasService {

  private SCORE_LOJAS = 'https://goanalytics.executeti.com.br/api/query/store/ranking/sales/by/total';

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getRanking() {
    return this.httpClient.get<RankingModel[]>(this.SCORE_LOJAS).pipe(retry(3),
      catchError(this.handleError));
  }
}
