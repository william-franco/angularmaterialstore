export interface ProductListModel {
    id: number;
    name: string;
    description: string;
    price: number;
    image: string;
    weight: string;
    length: string;
    height: string;
    width: string;
    stars: string;
    freeShipping: boolean;
}
