import { Component, OnInit } from '@angular/core';
import { RankingLojasService } from '../ranking-lojas.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { RankingModel } from '../ranking-model';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit {

  // scores: RankingModel[] = [];
  scores: [];
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private dataService: RankingLojasService) { }

  ngOnInit() {
    this.getRanking();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

  getRanking() {
    /*this.dataService.getRanking()
      .subscribe(resp => this.scores = resp as []);*/

    this.dataService.getRanking().subscribe((resp: RankingModel[]) => {
      console.log(resp);
      this.scores = resp as [];
    });

  }

}
