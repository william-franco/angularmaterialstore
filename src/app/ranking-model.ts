export interface RankingModel {
    id: number;
    count: string;
    name: string;
}
