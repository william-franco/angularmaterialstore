import { TestBed } from '@angular/core/testing';

import { RankingLojasService } from './ranking-lojas.service';

describe('RankingLojasService', () => {
  let service: RankingLojasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RankingLojasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
