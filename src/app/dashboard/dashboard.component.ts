import { Component, OnInit } from '@angular/core';
import { ValorTotalService } from '../valor-total.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  total_usuarios;
  total_lojas;
  total_produtos;
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private dataService: ValorTotalService) { }

  ngOnInit() {
    this.dataService.getUsers().subscribe((data: any[]) => {
      console.log(data);
      this.total_usuarios = data;
    });

    this.dataService.getLojasAtivas().subscribe((data: any[]) => {
      console.log(data);
      this.total_lojas = data;
    });

    this.dataService.getProdutosAtivos().subscribe((data: any[]) => {
      console.log(data);
      this.total_produtos = data;
    });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}
