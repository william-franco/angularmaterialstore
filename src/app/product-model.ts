export interface ProductModel {
    id: number;
    name: string;
    cnpj: string;
    cep: string;
    created: string;
    lastSyncDate: string;
    logo: string;
}
