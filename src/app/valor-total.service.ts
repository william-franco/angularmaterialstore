import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ValorTotalService {

  private TOTAL_USERS = 'https://goanalytics.executeti.com.br/api/customers/count';

  private TOTAL_LOJAS_ATIVAS = 'https://goanalytics.executeti.com.br/api/query/store/total/count/active';

  private TOTAL_PRODUTOS_ATIVOS = 'https://goanalytics.executeti.com.br/api/products/count';

  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getUsers(){
    return this.httpClient.get(this.TOTAL_USERS).pipe(retry(3),
    catchError(this.handleError));
  }

  public getLojasAtivas(){
    return this.httpClient.get(this.TOTAL_LOJAS_ATIVAS).pipe(retry(3),
    catchError(this.handleError));
  }

  public getProdutosAtivos(){
    return this.httpClient.get(this.TOTAL_PRODUTOS_ATIVOS).pipe(retry(3),
    catchError(this.handleError));
  }
}
