import { TestBed } from '@angular/core/testing';

import { ValorTotalService } from './valor-total.service';

describe('ValorTotalService', () => {
  let service: ValorTotalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValorTotalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
